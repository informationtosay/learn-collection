package learn_Collection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Map_Demo {

	public static void main(String[] args) {
		//TreeMap hm = new TreeMap(); // Ascending order maintain, same data type only allow, duplicate not allow
		//LinkedHashMap hm = new LinkedHashMap();// order maintain ,duplicate not allow
		HashMap hm = new HashMap();// key and value --> key duplicate not allow, value duplicate allow, not order maintain
		hm.put("idli", 30);
		hm.put("dosa", 30);
		hm.put("pori", 35);
		hm.put("pongal", 45);
		hm.put("vadai", 25);
		hm.put("meals", 50);
		hm.put("meals", 55);
		
		System.out.println(hm.containsKey("pongal"));// is there --> return boolean
		System.out.println(hm.containsValue(30));// is there --> return boolean
		System.out.println(hm.get("vadai"));// get key value
		System.out.println(hm.isEmpty());// check this object value is empty
		System.out.println(hm.putIfAbsent("meals", 40));// incase this key absent add to key
		System.out.println(hm.size());// size-->means = length of key
		System.out.println(hm.remove("vadai"));//key remove from map
		System.out.println(hm);// print total key {in this type }
		System.out.println(hm.entrySet());// get key and value [in this type]
		System.out.println(hm.keySet());// get [key only]
		System.out.println(hm.values());// get [value only]
		
		Set s = hm.entrySet();
		Iterator i = s.iterator();// iterator is same as looping concept
		while(i.hasNext()) {
			Map.Entry me =(Map.Entry)i.next(); // this is interface 
			System.out.println(me.getKey()); // get single key 
			System.out.println(me.getValue());// get single value
			//System.out.println(i.next());// print to use loop in separately(key and values)
		}

	}

}

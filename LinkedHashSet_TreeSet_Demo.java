package learn_Collection;

import java.util.LinkedHashSet;
import java.util.TreeSet;

public class LinkedHashSet_TreeSet_Demo { 

	public static void main(String[] args) {
		
		LinkedHashSet link = new LinkedHashSet();//insertion order maintain, duplicate removed
		link.add(30);
		link.add(12);
		link.add(10);
		link.add(12);
		link.add(10);
		link.add(12);

		System.out.println(link);
		
		TreeSet tree = new TreeSet();//ascending order maintain, duplicate removed
		tree.add(10);
		tree.add(5);
		tree.add(9);
		tree.add(11);
		tree.add(2);
		tree.add(4);
		tree.add(9);
		
		System.out.println(tree);
	
	}

}

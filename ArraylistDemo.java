package learn_Collection;

import java.util.ArrayList;

public class ArraylistDemo {

	public static void main(String[] args) {
		
		ArrayList al = new ArrayList();
		al.add(10); // auto boxing int --> Integer (Wrapper class)
		al.add(10.5);// auto boxing double --> Double
		al.add(true);// auto boxing boolean --> Boolean
		al.add("Ramu");// String	
		System.out.println(al);
		
		ArrayList al2 = new ArrayList();
		al2.addAll(al);// addAll method
		System.out.println(al2);
		
		ArrayList al3 = new ArrayList();
		al3.add(10); 
		; 
		al3.addAll(1,al);// index based addAll method
		System.out.println(al3);
		
		System.out.println(al3.contains(10)); // checking for same value in arraylist
		System.out.println(al3.containsAll(al2)); // checking for Allvalue same in arraylist
		System.out.println(al3.get(0)); // index based value
		System.out.println(al3.indexOf(10)); // which index on this value
		System.out.println(al3.lastIndexOf(10));// count index on this value reverse to measure
		System.out.println(al3.isEmpty());// check on this object isEmpty 
		System.out.println(al3.remove(0));// removing for this index value
		System.out.println(al3.remove(1));// removing for this index value
		System.out.println("sdasa"+al3.retainAll(al2));// 
        System.out.println(al3);
		al3.removeAll(al2); // removeAll value on this arraylist
		System.out.println(al3);
		System.out.println(al3.size());// size of arraylist (get length count)
		System.out.println(al.subList(1, 3));// 1 index to 2 index value get




			}
	public void addd (Object ad) {
		
	}


}

package learn_Collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class SetDemo {

	public static void main(String[] args) {
		
		HashSet hash = new HashSet(); // no insertion order maintain, no duplicate allow
		hash.add("ram");
		hash.add("thaniga");
		hash.add("ram");
		hash.add("siva");
		hash.add("stephen");
		hash.add("akash");  // index base not value get
		
		for(Object obj:hash) {  // another way for each use to index will get
			if(obj.equals("ram")) {
			System.out.println(obj);
			}
		}
		
		System.out.println(hash);
		
		ArrayList ar = new ArrayList();
		ar.add(50);
		ar.add(55);
		ar.add(50);
		ar.add(53);
		ar.add(52);
		ar.add(51);
//		Object[] ob =ar.toArray();
//		System.out.println(ob);
		
		HashSet hash2 = new HashSet(ar); // remove duplicate value
		System.out.println(hash2);

		//asList method use to array to arraylist convert
		Integer[] ar2 = {10,20,30,40,50,10,20,30};
		List ll = Arrays.asList(ar2);
		System.out.println(ll);
		HashSet hash3 = new HashSet(ll);
		System.out.println(hash3);

		//Collections.addAll() method use to array to arraylist convert
        String[] geeks = {"ram","siva","thaniga"};
        ArrayList al4 = new ArrayList();
        Collections.addAll(al4, geeks);
        System.out.println(al4);
		
	}

}
